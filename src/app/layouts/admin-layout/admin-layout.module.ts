import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { ClientesComponent } from '../../clientes/clientes.component';
import { FormComponent } from '../../clientes/form.component';

import { ProductosFormComponent } from '../../productos/productos-form.component';
import { DirectivaComponent } from '../../directiva/directiva.component';


import { ProductosComponent } from '../../productos/productos.component';
import { DetalleProveedorComponent } from '../../proveedores/detalle/detalle-proveedor.component';

import { DetalleComponent } from '../../clientes/detalle/detalle.component';
import { PaginatorComponent } from '../../paginator/paginator.component';
import { PaginatorProductosComponent } from '../../paginator-productos/paginator-productos.component';

import { PaginatorVentasComponent } from '../../paginator-ventas/paginator-ventas.component';

import { DetalleFacturaComponent } from '../../facturas/detalle-factura.component';
import { FacturaComponent } from '../../facturas/factura.component';
import { FacturasComponent } from '../../listado-factura/facturas.component';
import { ItemFacturaComponent } from '../../listado-item-factura/item-factura.component';
import { ListadoPedidosComponent } from '../../listado-pedidos/listado-pedidos.component';
import { ListadoPedidosVistaComponent } from '../../listado-pedidos-vista/listado-pedidos-vista.component';
import { UsuariosComponent } from '../../usuarios/usuarios.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatDatepickerModule } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { HomeComponent } from '../../home/home.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatFormFieldModule, MatInputModule }  from '@angular/material';

import { ProveedoresComponent } from '../../proveedores/proveedores.component';
import { FormProveedoresComponent } from '../../proveedores/form-proveedores.component';

import { DetalleCompraComponent } from '../../compras/detalle-compra.component';
import { CompraComponent } from '../../compras/compra.component';
import { ItemCompraComponent } from '../../listado-item-compra/item-compra.component';
import { ComprasComponent } from '../../listado-compra/compras.component';

import { DetalleEntradaComponent } from '../../entradas/detalle-entrada.component';
import { EntradaComponent } from '../../entradas/entrada.component';
import { EntradasComponent } from '../../listado-entradas/entradas.component';

import { UsuariosFormComponent } from '../../usuarios/usuarios-form.component';

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,MatDatepickerModule, MatMomentDateModule, MatAutocompleteModule, MatFormFieldModule, MatInputModule
  ],
  declarations: [
  
    DetalleComponent,
    DetalleProveedorComponent,
    PaginatorComponent,

    HomeComponent,
    FormComponent,
    ClientesComponent,
    ProductosFormComponent,
    DirectivaComponent,
    ProductosComponent,
    DetalleFacturaComponent,
    FacturaComponent,
    FacturasComponent,
    ItemFacturaComponent,
    ListadoPedidosComponent,
    ListadoPedidosVistaComponent,
    UsuariosComponent,
    PaginatorProductosComponent,
    PaginatorVentasComponent,
    CompraComponent,
    DetalleCompraComponent,
    FormProveedoresComponent,
    ProveedoresComponent,
    ItemCompraComponent,
    ComprasComponent,
    DetalleEntradaComponent,
    EntradaComponent,
    EntradasComponent,
    UsuariosFormComponent
  ],
  providers: [DatePipe]
})

export class AdminLayoutModule { }
