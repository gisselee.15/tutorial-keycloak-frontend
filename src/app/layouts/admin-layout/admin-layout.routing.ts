import { Routes } from '@angular/router';


import { ClientesComponent } from '../../clientes/clientes.component';
import { FormComponent } from '../../clientes/form.component';
//import { UsuariosFormComponent } from '../../usuarios/form.component';

import { ProductosFormComponent } from '../../productos/productos-form.component';
//import { FormRolesAgregarComponent } from '../../usuarios/form.roles.agregar.component';
import { DirectivaComponent } from '../../directiva/directiva.component';


import { ProductosComponent } from '../../productos/productos.component';


//import { UsuariosComponent } from '../../usuarios/usuarios.component';
import { FacturasComponent } from '../../listado-factura/facturas.component';
import { ItemFacturaComponent } from '../../listado-item-factura/item-factura.component';
import { ListadoPedidosComponent } from '../../listado-pedidos/listado-pedidos.component';

import { ComprasComponent } from '../../listado-compra/compras.component';

import { ProveedoresComponent } from '../../proveedores/proveedores.component';
import { FormProveedoresComponent } from '../../proveedores/form-proveedores.component';

import { ListadoPedidosVistaComponent } from '../../listado-pedidos-vista/listado-pedidos-vista.component';

import { UsuariosComponent } from '../../usuarios/usuarios.component';
import { UsuariosFormComponent } from '../../usuarios/usuarios-form.component';


import { DetalleFacturaComponent } from '../../facturas/detalle-factura.component';
import { FacturaComponent } from '../../facturas/factura.component';

import { DetalleEntradaComponent } from '../../entradas/detalle-entrada.component';
import { EntradaComponent } from '../../entradas/entrada.component';
import { DetalleCompraComponent } from '../../compras/detalle-compra.component';
import { CompraComponent } from '../../compras/compra.component';
import { ItemCompraComponent } from '../../listado-item-compra/item-compra.component';
import { EntradasComponent } from '../../listado-entradas/entradas.component';

import { HomeComponent } from '../../home/home.component';

export const AdminLayoutRoutes: Routes = [
  { path: 'home', component: HomeComponent },

  { path: 'directivas', component: DirectivaComponent },
  { path: 'clientes', component: ClientesComponent },
  { path: 'clientes/page/:page', component: ClientesComponent},
  { path: 'clientes/form', component: FormComponent},
  { path: 'clientes/form/:id', component: FormComponent },
  { path: 'productos', component: ProductosComponent },
  { path: 'productos/page/:page', component: ProductosComponent },
  { path: 'productos/form', component: ProductosFormComponent  },
  { path: 'productos/form/:id', component: ProductosFormComponent  },
/*{ path: 'usuarios', component: UsuariosComponent },
  { path: 'usuarios/page/:page', component: UsuariosComponent },
  { path: 'usuarios/form', component: UsuariosFormComponent  },
  { path: 'usuarios/form/:id', component: UsuariosFormComponent  },
  { path: 'usuarios/form/roles/agregar/:id', component: FormRolesAgregarComponent  },*/
  { path: 'facturas/:id', component: DetalleFacturaComponent },
  { path: 'facturas/form/:clienteId', component: FacturaComponent  },
  { path: 'facturas', component: FacturasComponent  },
  { path: 'facturas/page/:page', component: FacturasComponent },

  { path: 'listado-items-facturas', component: ItemFacturaComponent  },
  { path: 'listado-pedidos', component: ListadoPedidosComponent  },
  { path: 'listado-pedidos-vista', component: ListadoPedidosVistaComponent  },

  { path: 'proveedores', component: ProveedoresComponent },
  { path: 'proveedores/page/:page', component: ProveedoresComponent},
  { path: 'proveedores/form', component: FormProveedoresComponent},
  { path: 'proveedores/form/:id', component: FormProveedoresComponent },

  { path: 'compras/:id', component: DetalleCompraComponent },
  { path: 'compras/form/:proveedorId', component: CompraComponent  },
  { path: 'compras', component: ComprasComponent  },
  { path: 'compras/page/:page', component:CompraComponent },
  { path: 'listado-items-compras', component: ItemCompraComponent  },

  { path: 'entradas/:id', component: DetalleEntradaComponent },
  { path: 'entradas/form/:clienteId', component: EntradaComponent  },
  { path: 'entradas', component: EntradasComponent  },
  { path: 'usuarios', component: UsuariosComponent  },
  { path: 'usuarios/form', component: UsuariosFormComponent},
  { path: 'usuarios/form/:id', component: UsuariosFormComponent },
  
];
