import { Component, OnInit } from '@angular/core';
import { CompraService } from '../compras/services/compra.service';
import { Compra } from '../compras/models/compra';
import { ActivatedRoute } from '@angular/router';
import { map, catchError, tap } from 'rxjs/operators';
import { ItemCompra } from './models/items-compra';
import swal from 'sweetalert2';

@Component({
  selector: 'app-item-compra',
  templateUrl: './item-compra.component.html'
})
export class ItemCompraComponent implements OnInit {
  errores: string[];

  compra: Compra;
  titulo: string = 'Compra';
  itemCompra: ItemCompra[];
  paginador: any;
  constructor(private compraService: CompraService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    //cambiar a listado de compras 
    /*this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      this.compraService.getCompra(id).subscribe(compra => this.compra = compra);
    });*/

    this.listar();

   
  }
  listar(){
    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');

      if (!page) {
        page = 0;
      }

      this.compraService.getItemCompraPage(page)
        .pipe(
          tap(response => {
            console.log('CompraComponent: tap 3');
            (response.content as ItemCompra[]).forEach(itemCompra => console.log(itemCompra.importe));
          })
        ).subscribe(response => {
          this.itemCompra = response.content as ItemCompra[];
          this.paginador = response;
          if(response.content!=null){
            var list=response.content;
            var hay: any;
            hay=list.findIndex(i => i.estado === "pendiente");
            if(hay==-1)
            {
              this.ngOnInit();
            }
          }else{
            this.ngOnInit();
          }
        });
    });
  }
  procesar(itemCompra: ItemCompra): void {
    swal({
      title: 'Está seguro?',
      text: `¿Desea realizar el pedido ${itemCompra.id} del cliente ${itemCompra.nombre}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si!',
      cancelButtonText: 'No!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
          console.log(itemCompra);
          //this.cliente.compras = null;
          itemCompra.estado='en proceso';
          this.compraService.updateItem(itemCompra)
            .subscribe(
              json => {
                //this.router.navigate(['/clientes']);
                swal('Item Actualizado', `${json.mensaje}: ${itemCompra.id}-${itemCompra.nombre}`, 'success');
                this.ngOnInit();
              },
              err => {
                this.errores = err.error.errors as string[];
                console.error('Código del error desde el backend: ' + err.status);
                console.error(err.error.errors);
              }
            )
      
      }
    });
  }
  listo(itemCompra: ItemCompra): void {
    swal({
      title: 'Está seguro?',
      text: `¿Desea dar por hecho  el pedido ${itemCompra.id} del cliente ${itemCompra.nombre}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si!',
      cancelButtonText: 'No!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
          console.log(itemCompra);
          //this.cliente.compras = null;
          itemCompra.estado='listo';
          this.compraService.updateItem(itemCompra)
            .subscribe(
              json => {
                //this.router.navigate(['/clientes']);
                swal('Item Actualizado', `${json.mensaje}: ${itemCompra.id}-${itemCompra.nombre}`, 'success');
                this.ngOnInit();
              },
              err => {
                this.errores = err.error.errors as string[];
                console.error('Código del error desde el backend: ' + err.status);
                console.error(err.error.errors);
              }
            )
      
      }
    });
  }

}
