import { ItemCompra } from './items-compra';
import { Cliente } from '../../clientes/cliente';

export class Compra {
  id: number;
  descripcion: string;
  observacion: string;
  items: Array<ItemCompra> = [];
  cliente: Cliente;
  total: number;
  createAt: string;

  calcularGranTotal(): number {
    this.total = 0;
    this.items.forEach((item: ItemCompra) => {
      this.total += item.calcularImporte();
    });
    return this.total;
  }
}
