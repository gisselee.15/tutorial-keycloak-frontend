import { Producto } from './producto';

export class ItemCompra {
  id: number;
  producto: Producto;
  cantidad: number = 1;
  importe: number;

  compraId: number;
  nombre: string;
  descripcion: string;
  observacion: string;

  estado: string;
  public calcularImporte(): number {
    return this.cantidad * this.producto.precio;
  }
  /*
  "id": 9999,
            "cantidad": 5,
            "producto": {
                "id": 1,
                "nombre": "coca cola no retornable 500 ml",
                "precio": 10000.0,
                "createAt": null,
                "tipo": "bebida"
            },
            "estado": "entregado",
            "nombre": "angel",
            "importe": 50000.0,
            "compraId": 999999 */
}
