import { Component, OnInit, Input } from '@angular/core';
import { ClienteService } from '../clientes/cliente.service';
import { ModalService } from '../clientes/detalle/modal.service';
import { ActivatedRoute } from '@angular/router';

import swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { Cliente } from '../clientes/cliente';
import { map, catchError, tap } from 'rxjs/operators';

import { EntradaService } from '../entradas/services/entrada.service';
import { Entrada } from '../entradas/models/entrada';
@Component({
  selector: 'app-entradas',
  templateUrl: './listado-entrada.component.html'
})
export class EntradasComponent implements OnInit {

  @Input() cliente: Cliente;
  entradas: Entrada[];
  paginador: any;
  entradaSeleccionado: Entrada;

  constructor(private clienteService: ClienteService,
    private entradaService: EntradaService,
    private modalService: ModalService,
    private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      this.activatedRoute.paramMap.subscribe(params => {
        let page: number = +params.get('page');
  
        if (!page) {
          page = 0;
        }
  
        this.entradaService.getEntradaPage(page)
          .pipe(
            tap(response => {
              console.log('EntradaComponent: tap 3');
              (response.content as Entrada[]).forEach(entrada => console.log(entrada.descripcion));
            })
          ).subscribe(response => {
            this.entradas = response.content as Entrada[];
            this.paginador = response;
          });
      });
    }
    cerrarModal() {
      this.modalService.cerrarModal();
    }
  
    delete(entrada: Entrada): void {
      swal({
        title: 'Está seguro?',
        text: `¿Seguro que desea anular la entrada ${entrada.descripcion}?`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, anular!',
        cancelButtonText: 'No, cancelar!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
  
          this.entradaService.delete(entrada.id).subscribe(
            () => {
              //this.cliente.entradas = this.cliente.entradas.filter(f => f !== entrada)
              swal(
                'Entrada Eliminada!',
                `Entrada ${entrada.descripcion} eliminada con éxito.`,
                'success'
              )
            }
          )
  
        }
      });
    }

}
