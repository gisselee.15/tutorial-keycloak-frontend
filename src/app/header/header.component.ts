import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { LoginKeycloakService } from '../services/loginKeycloack/login-keycloak.service';
import { KeycloakService } from 'keycloak-angular';
import { LoginGuardGuard } from '../guards/login-guard.guard';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
  title: string = 'OLIVA APP'
  username: any = {};
  rol:any;
  roles:string;
  constructor( private router: Router, private loginService: LoginKeycloakService, private keycloak: KeycloakService) { }
  /*logout(): void {
    let username = this.authService.usuario.username;
    this.authService.logout();
    swal('Logout', `Hola ${username}, has cerrado sesión con éxito!`, 'success');
    this.router.navigate(['/login']);
  }*/
  ngOnInit(): void {
    this.username=this.keycloak.getUsername();
    this.roles=this.loginService.getRolUser();//realm-user, realm-admin
  }
 
  logout() {
    Swal({
     
      title: 'Desea salir del sistema?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si!',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.loginService.logout();
      }
    })
  }
}
