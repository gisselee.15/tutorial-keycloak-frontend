import { Component, OnInit, Input } from '@angular/core';
import { ClienteService } from '../clientes/cliente.service';
import { ModalService } from '../clientes/detalle/modal.service';
import { ActivatedRoute } from '@angular/router';

import swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { Cliente } from '../clientes/cliente';
import { map, catchError, tap } from 'rxjs/operators';
import { saveAs } from 'file-saver';
import { HttpResponse } from '@angular/common/http';

import { FacturaService } from '../facturas/services/factura.service';
import { Factura } from '../facturas/models/factura';
@Component({
  selector: 'app-facturas',
  templateUrl: './listado-factura.component.html'
})
export class FacturasComponent implements OnInit {

  @Input() cliente: Cliente;
  facturas: Factura[];
  paginador: any;
  facturaSeleccionado: Factura;
  fechaDesde: string;
  fechaHasta: string;
  facturasFechas: Factura[];
  
  file: any = {};
  constructor(private clienteService: ClienteService,
    private facturaService: FacturaService,
    private modalService: ModalService,
    private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      this.activatedRoute.paramMap.subscribe(params => {
        let page: number = +params.get('page');
  
        if (!page) {
          page = 0;
        }
  
        this.facturaService.getFacturaPage(page)
          .pipe(
            tap(response => {
              console.log('FacturaComponent: tap 3');
              (response.content as Factura[]).forEach(factura => console.log(factura.descripcion));
            })
          ).subscribe(response => {
            this.facturas = response.content as Factura[];
            this.paginador = response;
          });
      });
    }
    cerrarModal() {
      this.modalService.cerrarModal();
    }
  
    delete(factura: Factura): void {
      swal({
        title: 'Está seguro?',
        text: `¿Seguro que desea anular la factura ${factura.descripcion}?`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, anular!',
        cancelButtonText: 'No, cancelar!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
  
          this.facturaService.delete(factura.id).subscribe(
            () => {
              this.cliente.facturas = this.cliente.facturas.filter(f => f !== factura)
              swal(
                'Factura Eliminada!',
                `Factura ${factura.descripcion} eliminada con éxito.`,
                'success'
              )
            }
          )
  
        }
      });
    }
    reporteFactura(): void {
      this.facturaService.findAllFechas(this.fechaDesde, this.fechaHasta).subscribe(
        
          (res: HttpResponse<any>) => {
            this.file = res;
            const contentDispositionHeader = res.headers.get('content-disposition')
            if (contentDispositionHeader !== null || contentDispositionHeader != undefined) {
              const contentDispositionHeaderResult = contentDispositionHeader.split(';')[1].trim().split('=')[1];
              const contentDispositionFileName = contentDispositionHeaderResult.replace(/"/g, '');
              saveAs(this.file.body, contentDispositionFileName);
              swal.close();
  
            } else {
              swal(
                'Sin archivo adjunto',
                `Sin archivo adjunto`,
                'warning'
              )
            }
          }
          ,
          err => {           
            swal(
              'Sin archivo adjunto',
              `Sin archivo adjunto`,
              'error'
            )
            swal.close();
          }
        );      
    }
    reporteFacturaPdf(): void {
      this.facturaService.findAllFechasPdf(this.fechaDesde, this.fechaHasta).subscribe(
        
          (res: HttpResponse<any>) => {
            this.file = res;
            const contentDispositionHeader = res.headers.get('content-disposition')
            if (contentDispositionHeader !== null || contentDispositionHeader != undefined) {
              const contentDispositionHeaderResult = contentDispositionHeader.split(';')[1].trim().split('=')[1];
              const contentDispositionFileName = contentDispositionHeaderResult.replace(/"/g, '');
              saveAs(this.file.body, contentDispositionFileName);
              swal.close();
  
            } else {
              swal(
                'Sin archivo adjunto',
                `Sin archivo adjunto`,
                'warning'
              )
            }
          }
          ,
          err => {           
            swal(
              'Sin archivo adjunto',
              `Sin archivo adjunto`,
              'error'
            )
            swal.close();
          }
        );      
    }
  
}
