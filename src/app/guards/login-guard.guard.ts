/**
 * @author Sergio Amarilla
 * @since 26-03-2021
 * @package para verificar si esta o no logueado al sistema y realizar el redirect a keycloak
 */

import { Injectable } from '@angular/core';
import {  ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { KeycloakAuthGuard, KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})


export class LoginGuardGuard extends KeycloakAuthGuard {
  constructor(protected router: Router, protected keycloakAngular: KeycloakService) {
    super(router, keycloakAngular);
  }
  isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean | UrlTree> {
    return new Promise(async (resolve, reject) => {
      if (!this.authenticated) {
        this.keycloakAngular.login();
        resolve(false);
        return;
      }
      const requierdRoles = route.data.roles;
      let granted: boolean = false;
      if (!requierdRoles || requierdRoles.length === 0) {
        granted = true;
      } else {
        for (const requierdRole of requierdRoles) {
          if (this.roles.indexOf(requierdRoles) > -1) {
            granted = true;
          }
        }
      }
      if (granted == false) {
        resolve(granted);
      }
      resolve(granted);
    })
  }
  getRol() {
    return this.roles;
  }
}
