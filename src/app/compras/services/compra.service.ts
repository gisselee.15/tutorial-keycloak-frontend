import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Compra } from '../models/compra';
import { Producto } from '../models/producto';
import { map, catchError, tap } from 'rxjs/operators';
import { ItemCompra } from '../../listado-item-compra/models/items-compra';

@Injectable({
  providedIn: 'root'
})
export class CompraService {

  private urlEndPoint: string = 'http://localhost:8080/api/compras';
  private urlEndPointItem: string = 'http://localhost:8080/api/item-compra';

  constructor(private http: HttpClient) { }
 
  findAllFechas(fecha1: string, fecha2: string): Observable<any> { //PORBAR 
    return this.http.get<Blob>(this.urlEndPoint+ '/findAllFechas/' + fecha1 + '/'+fecha2, {
      observe: 'response', responseType: 'blob' as 'json'
    })
  }

  getCompraPage(page: number): Observable<any> {
    return this.http.get(this.urlEndPoint + '/page/' + page).pipe(
      tap((response: any) => {
        console.log('compraService: tap 1');
        (response.content as Compra[]).forEach(compra => console.log(compra.descripcion));
      }),
      map((response: any) => {
        (response.content as Compra[]).map(compra => {
          compra.descripcion = compra.descripcion.toUpperCase();
          return compra;
        });
        return response;
      }),
      tap(response => {
        console.log('CompraService: tap 2');
        (response.content as Compra[]).forEach(compra => console.log(compra.descripcion));
      }));
  }
  getItemCompraPage(page: number): Observable<any> {
    return this.http.get(this.urlEndPointItem + '/page/' + page).pipe(
      tap((response: any) => {
        console.log('compraService: tap 1');
        (response.content as ItemCompra[]).forEach(itemCompra => console.log(itemCompra.compraId));
      }),
      map((response: any) => {
        (response.content as ItemCompra[]).map(itemCompra => {
          itemCompra.estado =  itemCompra.estado;
          return itemCompra;
        });
        return response;
      }),
      tap(response => {
        console.log('CompraService: tap 2');
        (response.content as ItemCompra[]).forEach(itemCompra => console.log(itemCompra.estado));
      }));
  }
  getCompra(id: number): Observable<Compra> {
    return this.http.get<Compra>(`${this.urlEndPoint}/${id}`);
  }
  updateItem(cliente: ItemCompra): Observable<any> {
    return this.http.put<any>(`${this.urlEndPointItem}/${cliente.id}`, cliente).pipe(
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }));
  }
  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.urlEndPoint}/${id}`);
  }

  filtrarProductos(term: string): Observable<Producto[]> {
    return this.http.get<Producto[]>(`${this.urlEndPoint}/filtrar-productos/${term}`);
  }

  create(compra: Compra): Observable<Compra> {
    return this.http.post<Compra>(this.urlEndPoint, compra);
  }
}
