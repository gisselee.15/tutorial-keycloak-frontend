import { Component, OnInit } from '@angular/core';
import { CompraService } from './services/compra.service';
import { Compra } from './models/compra';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalle-compra',
  templateUrl: './detalle-compra.component.html'
})
export class DetalleCompraComponent implements OnInit {

  compra: Compra;
  titulo: string = 'Compra';

  constructor(private compraService: CompraService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      this.compraService.getCompra(id).subscribe(compra =>
         this.compra = compra);
    });
  }

}
