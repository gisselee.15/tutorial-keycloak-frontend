import { Component, OnInit } from '@angular/core';
import { Compra } from './models/compra';
import { ProveedorService } from '../proveedores/proveedor.service';
import { ActivatedRoute, Router } from '@angular/router';

import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
//import { map, startWith } from 'rxjs/operators';

import { map, flatMap, startWith } from 'rxjs/operators';
import { CompraService } from './services/compra.service';
import { Producto } from './models/producto';
import { ItemCompra } from './models/item-compra';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import swal from 'sweetalert2';

@Component({
  selector: 'app-compra',
  templateUrl: './compra.component.html'
})
export class CompraComponent implements OnInit {

  titulo: string = 'Nueva Compra';
  compra: Compra = new Compra();

  autocompleteControl = new FormControl();

  productosFiltrados: Observable<Producto[]>;

  constructor(private proveedorService: ProveedorService,
    private compraService: CompraService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let proveedorId = +params.get('proveedorId');
      this.proveedorService.getProveedor(proveedorId).subscribe(proveedor => this.compra.proveedor = proveedor);
    });

    this.productosFiltrados = this.autocompleteControl.valueChanges
      .pipe(
        map(value => typeof value === 'string' ? value : value.nombre),
        flatMap(value => value ? this._filter(value) : [])
      );
  }

  private _filter(value: string): Observable<Producto[]> {
    const filterValue = value.toLowerCase();

    return this.compraService.filtrarProductos(filterValue);
  }

  mostrarNombre(producto?: Producto): string | undefined {
    return producto ? producto.nombre : undefined;
  }

  seleccionarProducto(event: MatAutocompleteSelectedEvent): void {
    let producto = event.option.value as Producto;
    console.log(producto);

    if (this.existeItem(producto.id)) {
      this.incrementaCantidad(producto.id);
    } else {
      let nuevoItem = new ItemCompra();
      nuevoItem.producto = producto;
      this.compra.items.push(nuevoItem);
    }

    this.autocompleteControl.setValue('');
    event.option.focus();
    event.option.deselect();

  }

  actualizarCosto(id: number, event: any): void {
    let costo: number = event.target.value as number;

    this.compra.items = this.compra.items.map((item: ItemCompra) => {
      if (id === item.producto.id) {
        item.costo = costo;
      }
      return item;
    });
  }
  actualizarCantidad(id: number, event: any): void {
    let cantidad: number = event.target.value as number;

    if (cantidad == 0) {
      return this.eliminarItemCompra(id);
    }

    this.compra.items = this.compra.items.map((item: ItemCompra) => {
      if (id === item.producto.id) {
        item.cantidad = cantidad;
      }
      return item;
    });
  }

  existeItem(id: number): boolean {
    let existe = false;
    this.compra.items.forEach((item: ItemCompra) => {
      if (id === item.producto.id) {
        existe = true;
      }
    });
    return existe;
  }

  incrementaCantidad(id: number): void {
    this.compra.items = this.compra.items.map((item: ItemCompra) => {
      if (id === item.producto.id) {
        ++item.cantidad;
      }
      return item;
    });
  }

  eliminarItemCompra(id: number): void {
    this.compra.items = this.compra.items.filter((item: ItemCompra) => id !== item.producto.id);
  }

  create(compraForm): void {
    console.log(this.compra);
    this.compra.calcularGranTotal();
    if (this.compra.items.length == 0) {
      this.autocompleteControl.setErrors({ 'invalid': true });
    }

    if (compraForm.form.valid && this.compra.items.length > 0) {
      this.compraService.create(this.compra).subscribe(compra => {
        swal(this.titulo, `Compra ${compra.id} creada con éxito, Proveedor: ${compra.proveedor.nombre}, Encargado: ${compra.proveedor.encargado}!`, 'success');
        this.router.navigate(['/proveedores']);
      });
    }
  }

}
