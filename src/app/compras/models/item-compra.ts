import { Producto } from './producto';
import { Compra } from './compra';

export class ItemCompra {
  producto: Producto;
  cantidad: number = 1;
  importe: number=0;
  costo: number=0;
 
  public calcularImporte(): number {
    this.importe = 0;
    this.importe = this.cantidad * this.costo;
  
    return this.importe;  
  }

}
