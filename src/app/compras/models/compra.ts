import { ItemCompra } from './item-compra';
import { Proveedor } from '../../proveedores/proveedor';

export class Compra {
  id: number;
  descripcion: string;
  observacion: string;
  items: Array<ItemCompra> = [];
  proveedor: Proveedor;
  total: number;
  createAt: string;

  calcularGranTotal(): number {
    this.total = 0;
    this.items.forEach((item: ItemCompra) => {
      this.total += item.calcularImporte();
    });
    return this.total;
  }
}
