import { Component, OnInit, Input } from '@angular/core';
import { ProveedorService } from '../proveedores/proveedor.service';
import { ModalProveedorService } from '../proveedores/detalle/modal-proveedor.service';
import { ActivatedRoute } from '@angular/router';

import swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { Proveedor } from '../proveedores/proveedor';
import { map, catchError, tap } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

import { CompraService } from '../compras/services/compra.service';
import { Compra } from '../compras/models/compra';
@Component({
  selector: 'app-compras',
  templateUrl: './listado-compra.component.html'
})
export class ComprasComponent implements OnInit {

  @Input() proveedor: Proveedor;
  compras: Compra[];
  paginador: any;
  compraSeleccionado: Compra;
  fechaDesde: string;
  fechaHasta: string;
  file: any = {};
  constructor(private proveedorService: ProveedorService,
    private compraService: CompraService,
    private modalService: ModalProveedorService,
    private activatedRoute: ActivatedRoute) { }

    ngOnInit() {
      this.activatedRoute.paramMap.subscribe(params => {
        let page: number = +params.get('page');
  
        if (!page) {
          page = 0;
        }
  
        this.compraService.getCompraPage(page)
          .pipe(
            tap(response => {
              console.log('CompraComponent: tap 3');
              (response.content as Compra[]).forEach(compra => console.log(compra.descripcion));
            })
          ).subscribe(response => {
            this.compras = response.content as Compra[];
            this.paginador = response;
          });
      });
    }
    cerrarModal() {
      this.modalService.cerrarModal();
    }
  
    delete(compra: Compra): void {
      swal({
        title: 'Está seguro?',
        text: `¿Seguro que desea eliminar la compra ${compra.descripcion}?`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText: 'No, cancelar!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
  
          this.compraService.delete(compra.id).subscribe(
            () => {
              this.proveedor.compras = this.proveedor.compras.filter(f => f !== compra)
              swal(
                'Compra Eliminada!',
                `Compra ${compra.descripcion} eliminada con éxito.`,
                'success'
              )
            }
          )
  
        }
      });
    }
    reporteCompra(): void {
      this.compraService.findAllFechas(this.fechaDesde, this.fechaHasta).subscribe(
        
          (res: HttpResponse<any>) => {
            this.file = res;
            const contentDispositionHeader = res.headers.get('content-disposition')
            if (contentDispositionHeader !== null || contentDispositionHeader != undefined) {
              const contentDispositionHeaderResult = contentDispositionHeader.split(';')[1].trim().split('=')[1];
              const contentDispositionFileName = contentDispositionHeaderResult.replace(/"/g, '');
              saveAs(this.file.body, contentDispositionFileName);
              swal.close();
  
            } else {
              swal(
                'Sin archivo adjunto',
                `Sin archivo adjunto`,
                'warning'
              )
            }
          }
          ,
          err => {           
            swal(
              'Sin archivo adjunto',
              `Sin archivo adjunto`,
              'error'
            )
            swal.close();
          }
      );      
    }

}
