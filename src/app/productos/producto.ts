export class Producto {
  id: number;
  nombre: string;
  precio: number;
  createAt: string;
  tipo: string;
  existente: number;
  medida: string;

}
