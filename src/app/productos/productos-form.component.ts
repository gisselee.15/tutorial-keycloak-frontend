import { Component, OnInit } from '@angular/core';
import { Producto } from './producto';
import { ProductoService } from './producto.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-productos-form',
  templateUrl: './productos-form.component.html'
})
export class ProductosFormComponent implements OnInit {
  private producto: Producto = new Producto();
  tipos: string[]=['insumos','preparados','almacen','otros'];
  medidas: string[]=['kg','paquete','lt','unidad','docena', 'pack de 6', 'pack de 3', 'caja'];

  titulo: string = "Crear Producto";
  errores: string[];
  constructor(private productoService: ProductoService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  	 this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      if (id) {
        this.productoService.getProducto(id).subscribe((producto) => this.producto = producto);
      }
    });
    
  }
  create(): void {
    this.producto.existente=0;
    console.log(this.producto);
    this.productoService.create(this.producto)
      .subscribe(
        producto => {
          this.router.navigate(['/productos']);
          swal('Nuevo producto', `El producto ${producto.nombre} ha sido creado con éxito`, 'success');
        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      );
  } 
  update(): void {
    console.log(this.producto);
    this.productoService.update(this.producto)
      .subscribe(
        json => {
          this.router.navigate(['/productos']);
          swal('Producto Actualizado', `El producto  ha sido actualizado con éxito`, 'success');
        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      )
  }

}
