import { Component, OnInit } from '@angular/core';
import { FacturaService } from '../facturas/services/factura.service';
import { Factura } from '../facturas/models/factura';
import { ActivatedRoute } from '@angular/router';
import { map, catchError, tap } from 'rxjs/operators';
import { ItemFactura } from './models/items-factura';
import swal from 'sweetalert2';

@Component({
  selector: 'app-item-factura',
  templateUrl: './item-factura.component.html'
})
export class ItemFacturaComponent implements OnInit {
  errores: string[];

  factura: Factura;
  titulo: string = 'Factura';
  itemFactura: ItemFactura[];
  paginador: any;
  id: any;
  constructor(private facturaService: FacturaService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    //cambiar a listado de facturas 
    /*this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      this.facturaService.getFactura(id).subscribe(factura => this.factura = factura);
    });*/

  
 //DESCOMENTAR funcion  setInterval PARA CARGAR LISTA ACTUALZIADA 

    this.id = setInterval(() => {
        this.listar();
    }, 5000);
   
  }
  listar(){
    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');

      if (!page) {
        page = 0;
      }

      this.facturaService.getItemFacturaPage(page)
        .pipe(
          tap(response => {
            console.log('FacturaComponent: tap 3');
            (response.content as ItemFactura[]).forEach(itemFactura => console.log(itemFactura.importe));
          })
        ).subscribe(response => {
          this.itemFactura = response.content as ItemFactura[];
          this.paginador = response;
          if(response.content!=null){
            var list=response.content;
            var hay: any;
            hay=list.findIndex(i => i.estado === "pendiente");
            if(hay==-1)
            {
              this.id = setInterval(() => {
                
              }, 7000);
              
            }
          }else{
            this.id = setInterval(() => {
             
            }, 7000);
          }
        });
    });
  }
  procesar(itemFactura: ItemFactura): void {
    swal({
      title: 'Está seguro?',
      text: `¿Desea realizar el pedido ${itemFactura.id} del cliente ${itemFactura.nombre}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si!',
      cancelButtonText: 'No!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
          console.log(itemFactura);
          //this.cliente.facturas = null;
          itemFactura.estado='en proceso';
          this.facturaService.updateItem(itemFactura)
            .subscribe(
              json => {
                //this.router.navigate(['/clientes']);
                swal('Item Actualizado', `${json.mensaje}: ${itemFactura.id}-${itemFactura.nombre}`, 'success');
                this.ngOnInit();
              },
              err => {
                this.errores = err.error.errors as string[];
                console.error('Código del error desde el backend: ' + err.status);
                console.error(err.error.errors);
              }
            )
      
      }
    });
  }
  listo(itemFactura: ItemFactura): void {
    swal({
      title: 'Está seguro?',
      text: `¿Desea dar por hecho  el pedido ${itemFactura.id} del cliente ${itemFactura.nombre}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si!',
      cancelButtonText: 'No!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
          console.log(itemFactura);
          //this.cliente.facturas = null;
          itemFactura.estado='listo';
          this.facturaService.updateItem(itemFactura)
            .subscribe(
              json => {
                //this.router.navigate(['/clientes']);
                swal('Item Actualizado', `${json.mensaje}: ${itemFactura.id}-${itemFactura.nombre}`, 'success');
                this.ngOnInit();
              },
              err => {
                this.errores = err.error.errors as string[];
                console.error('Código del error desde el backend: ' + err.status);
                console.error(err.error.errors);
              }
            )
      
      }
    });
  }

}
