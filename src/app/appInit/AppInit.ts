import { environment } from "../../environments/environment";
import { KeycloakService } from "keycloak-angular";

//  export function initializer ( keycloak : KeycloakService) : ()  => Promise<any>{
//    const options :KeycloakOptions = {
//      config : environment.keycloakConfig
//    };
//    return (): Promise<any> => keycloak.init(options);
//  }


export function initializeKeycloak(keycloak: KeycloakService) {
  return ():  Promise<any>  =>
    keycloak.init({//new Keycloak('http://localhost:8080/myapp/keycloak.json');
      config: environment.keycloakConfig,
      initOptions: {
       // onLoad: 'check-sso',
        //silentCheckSsoRedirectUri: window.location.origin + '/assets/silent-check-sso.html',
        flow: 'implicit',
      },
    });
    
}