import { Component, OnInit, Output ,EventEmitter } from '@angular/core';
import { Factura } from './models/factura';
import { ClienteService } from '../clientes/cliente.service';
import { ActivatedRoute, Router } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
//import { map, startWith } from 'rxjs/operators';

import { map, flatMap, startWith, catchError, tap  } from 'rxjs/operators';
import { FacturaService } from './services/factura.service';
import { Producto } from './models/producto';
import { Caja } from './models/caja';

import { ItemFactura } from './models/item-factura';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import swal from 'sweetalert2';
import { KeycloakService } from 'keycloak-angular';

@Component({
  selector: 'app-factura',
  templateUrl: './factura.component.html'
})
export class FacturaComponent implements OnInit {
  @Output() voted = new EventEmitter<boolean>();
  cajalista: Caja[];
  closeResult:string;
  cajaAbierta: Caja;
  montoinicial: number;
  caja: Caja= new Caja();
  titulo: string = 'Nueva Factura';
  factura: Factura = new Factura();
  username: any = {};
  autocompleteControl = new FormControl();
  disabledApertura: boolean=true;
  disabledCierre: boolean=true;
  disabledForm: boolean=true;
  productosFiltrados: Observable<Producto[]>;

  constructor(private clienteService: ClienteService,
    private facturaService: FacturaService,
    private router: Router,
    private activatedRoute: ActivatedRoute, private keycloak: KeycloakService,private modalService: NgbModal) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let clienteId = +params.get('clienteId');
      this.clienteService.getCliente(clienteId).subscribe(cliente => this.factura.cliente = cliente);
    });

    this.productosFiltrados = this.autocompleteControl.valueChanges
      .pipe(
        map(value => typeof value === 'string' ? value : value.nombre),
        flatMap(value => value ? this._filter(value) : [])
      );
    
    this.username=this.keycloak.getUsername();
    //llamar a servicio que pregunta si 
    this.invocacionCaja();
  }
  invocacionCaja() {
    this.facturaService.getCaja(this.username)
    .pipe(
      tap(response => {})
    ).subscribe(response => {
      this.cajalista = response as Caja[];  
      if(this.cajalista.length>0){
        this.cajaAbierta=this.cajalista[0];
        if(this.cajaAbierta.estado=="cerrado"){
          //crear caja por usuario 
          this.disabledApertura=false;

          this.disabledForm=true;

          this.disabledCierre=true;

        }else{
          this.disabledForm=false;
          this.disabledCierre=false;
          this.disabledApertura=true;


        }
      }else{
        //se crea 
        this.disabledApertura=false;
        
        this.disabledForm=true;

        this.disabledCierre=true;
      }    
    });
    
    
  }

  mostrarCierre() {
    this.facturaService.getSumatoriaVentas(this.username, this.cajaAbierta.inicialAt )
    .pipe(
      tap(response => {})
    ).subscribe(response => {
      this.cajaAbierta.ventastotal = response;  
      this.cajaAbierta.montofinal = response + this.cajaAbierta.montoinicial;
    });
    
    
  }
  createCaja(): void {
    if (this.montoinicial < 0 || this.montoinicial==null ) {
      this.autocompleteControl.setErrors({ 'invalid': true });
    }
    this.caja.montoinicial=this.montoinicial;
    this.caja.usuario=this.username;
    this.caja.estado="abierto";

    this.facturaService.createCaja(this.caja).subscribe(caja => {
      swal('Apertura de CAJA', `Se realizo la apertura de caja con éxito. Usuario ${this.username} con monto inicial ${this.montoinicial} !`, 'success');
      this.disabledApertura=true;
      this.disabledForm=false;
      this.disabledCierre=false;
    });
    
  }

  open(content) {
   
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.createCaja();
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  openCierre(content) {
    this.mostrarCierre();
    this.cajaAbierta.estado="cerrado";
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
     // this.updateCaja();  //aca debe ir el update de caja con los datos 
     this.cajaAbierta.estado="cerrado";
     this.facturaService.updateCaja(this.cajaAbierta).subscribe(factura => {
      swal('Cierre de Caja', `Se ha realizado el Cierre de Caja con éxito, Usuario ${this.username} !`, 'success');
      this.invocacionCaja();
    });
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private _filter(value: string): Observable<Producto[]> {
    const filterValue = value.toLowerCase();

    return this.facturaService.filtrarProductos(filterValue);
  }

  mostrarNombre(producto?: Producto): string | undefined {
    return producto ? producto.nombre : undefined;
  }

  seleccionarProducto(event: MatAutocompleteSelectedEvent): void {
    let producto = event.option.value as Producto;
    console.log(producto);

    if (this.existeItem(producto.id)) {
      this.incrementaCantidad(producto.id);
    } else {
      let nuevoItem = new ItemFactura();
      nuevoItem.producto = producto;
      this.factura.items.push(nuevoItem);
    }

    this.autocompleteControl.setValue('');
    event.option.focus();
    event.option.deselect();

  }

  actualizarCantidad(id: number, event: any): void {
    let cantidad: number = event.target.value as number;

    if (cantidad == 0) {
      return this.eliminarItemFactura(id);
    }

    this.factura.items = this.factura.items.map((item: ItemFactura) => {
      if (id === item.producto.id) {
        item.cantidad = cantidad;
      }
      return item;
    });
  }

  actualizarImporte(id: number, event: any): void {
    let importe: number = event.target.value as number;

    if (importe == 0) {
      return this.eliminarItemFactura(id);
    }

    this.factura.items = this.factura.items.map((item: ItemFactura) => {
      if (id === item.producto.id) {
        item.importe = importe;
      }
      return item;
    });
  }
  existeItem(id: number): boolean {
    let existe = false;
    this.factura.items.forEach((item: ItemFactura) => {
      if (id === item.producto.id) {
        existe = true;
      }
    });
    return existe;
  }

  incrementaCantidad(id: number): void {
    this.factura.items = this.factura.items.map((item: ItemFactura) => {
      if (id === item.producto.id) {
        ++item.cantidad;
      }
      return item;
    });
  }

  eliminarItemFactura(id: number): void {
    this.factura.items = this.factura.items.filter((item: ItemFactura) => id !== item.producto.id);
  }

  create(facturaForm): void {
    
    console.log(this.factura);
    if (this.factura.items.length == 0) {
      this.autocompleteControl.setErrors({ 'invalid': true });
    }
    this.factura.usuario=this.username;
    if (facturaForm.form.valid && this.factura.items.length > 0) {
      this.facturaService.create(this.factura).subscribe(factura => {
        swal(this.titulo, `Ticket ${factura.id} creada con éxito, Cliente ${factura.cliente.nombre} ${factura.cliente.apellido}!`, 'success');
        this.factura = new Factura();
        this.ngOnInit();
        this.voted.emit(true);
      });
    }
  }

}
