import { Producto } from './producto';
import { Factura } from './factura';

export class ItemFactura {
  producto: Producto;
  cantidad: number = 1;
  importe: number;

  public calcularImporte(): number {
    this.importe = 0;
    this.importe = this.cantidad * this.producto.precio;
  
    return this.importe;  
  }

  
}
