export class Caja {
  id: number;
  usuario: string;
  montoinicial: number;
  montofinal: number;
  ventastotal: number;
  inicialAt: string;
  finalAt: string;
  estado:string;
}
