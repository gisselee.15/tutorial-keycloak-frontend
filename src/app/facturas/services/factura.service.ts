import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Factura } from '../models/factura';
import { Caja } from '../models/caja';

import { Producto } from '../models/producto';
import { map, catchError, tap } from 'rxjs/operators';
import { ItemFactura } from '../../listado-item-factura/models/items-factura';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  private urlEndPoint: string = 'http://localhost:8080/api/facturas';
  private urlEndPointItem: string = 'http://localhost:8080/api/item-factura';

  private urlEndPointCaja: string = 'http://localhost:8080/api/caja';
  constructor(private http: HttpClient) { }

  
  findAllFechas(fecha1: string, fecha2: string): Observable<any> { 
    return this.http.get<Blob>(this.urlEndPoint+ '/findAllFechas/' + fecha1 + '/'+fecha2, {
      observe: 'response', responseType: 'blob' as 'json'
    })
  }


  findAllFechasPdf(fecha1: string, fecha2: string): Observable<any> { 
    return this.http.get<Blob>(this.urlEndPoint+ '/findAllFechasPdf/' + fecha1 + '/'+fecha2, {
      observe: 'response', responseType: 'blob' as 'json'
    })
  }
  getFacturaPage(page: number): Observable<any> {
    return this.http.get(this.urlEndPoint + '/page/' + page).pipe(
      tap((response: any) => {
        console.log('facturaService: tap 1');
        (response.content as Factura[]).forEach(factura => console.log(factura.descripcion));
      }),
      map((response: any) => {
        (response.content as Factura[]).map(factura => {
          factura.descripcion = factura.descripcion.toUpperCase();
          return factura;
        });
        return response;
      }),
      tap(response => {
        console.log('FacturaService: tap 2');
        (response.content as Factura[]).forEach(factura => console.log(factura.descripcion));
      }));
  }
  getItemFacturaPage(page: number): Observable<any> {
    return this.http.get(this.urlEndPointItem + '/page/' + page).pipe(
      tap((response: any) => {
        console.log('facturaService: tap 1');
        (response.content as ItemFactura[]).forEach(itemFactura => console.log(itemFactura.facturaId));
      }),
      map((response: any) => {
        (response.content as ItemFactura[]).map(itemFactura => {
          itemFactura.estado =  itemFactura.estado;
          return itemFactura;
        });
        return response;
      }),
      tap(response => {
        console.log('FacturaService: tap 2');
        (response.content as ItemFactura[]).forEach(itemFactura => console.log(itemFactura.estado));
      }));
  }
  getFactura(id: number): Observable<Factura> {
    return this.http.get<Factura>(`${this.urlEndPoint}/${id}`);
  }

  getCaja(username: string): Observable<any> {
    return this.http.get(this.urlEndPointCaja + '/username/' + username).pipe(
      tap((response: any) => {}),
      map((response: any) => {
        (response as Caja[]).map(caja => {
          return caja;
        });
        return response;
      }),
      tap(response => { }));
  }
  getSumatoriaVentas(username: string, fecha: string): Observable<any> { //PORBAR 
    return this.http.get(this.urlEndPointCaja+ '/ventas/' + username + '/'+fecha).pipe(
      tap((response: any) => {}),
      map((response: any) => {
        
        return response;
      }),
      tap(response => { }));
  }
  updateItem(cliente: ItemFactura): Observable<any> {
    return this.http.put<any>(`${this.urlEndPointItem}/${cliente.id}`, cliente).pipe(
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }));
  }
  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.urlEndPoint}/${id}`);
  }

  filtrarProductos(term: string): Observable<Producto[]> {
    return this.http.get<Producto[]>(`${this.urlEndPoint}/filtrar-productos/${term}`);
  }

  create(factura: Factura): Observable<Factura> {
    return this.http.post<Factura>(this.urlEndPoint, factura);
  }

  createCaja(caja: Caja): Observable<Caja> {
    return this.http.post<Caja>(this.urlEndPointCaja, caja);
  }

  updateCaja(cliente: Caja): Observable<any> {
    return this.http.put<any>(`${this.urlEndPointCaja}/${cliente.id}`, cliente).pipe(
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }));
  }
}
