import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, APP_INITIALIZER } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DirectivaComponent } from './directiva/directiva.component';

import { ClientesComponent } from './clientes/clientes.component';
import { FormComponent } from './clientes/form.component';
//import { UsuariosFormComponent } from './usuarios/form.component';

import { ProductosFormComponent } from './productos/productos-form.component';
//import { FormRolesAgregarComponent } from './usuarios/form.roles.agregar.component';


import { PaginatorComponent } from './paginator/paginator.component';
import { ClienteService } from './clientes/cliente.service';
import { ProductoService } from './productos/producto.service';

import { ProveedorService } from './proveedores/proveedor.service';

import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { registerLocaleData } from '@angular/common';
import localeES from '@angular/common/locales/es';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatDatepickerModule } from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { DetalleComponent } from './clientes/detalle/detalle.component';
import { DetalleProveedorComponent } from './proveedores/detalle/detalle-proveedor.component';

/*
import { DetalleRolesComponent } from './usuarios/detalle-roles/detalle.roles.component';

import { LoginComponent } from './usuarios/login.component';
import { AuthGuard } from './usuarios/guards/auth.guard';
import { RoleGuard } from './usuarios/guards/role.guard';
import { TokenInterceptor } from './usuarios/interceptors/token.interceptor';
import { AuthInterceptor } from './usuarios/interceptors/auth.interceptor';*/
import { ProductosComponent } from './productos/productos.component';


//import { UsuariosComponent } from './usuarios/usuarios.component';

import { DetalleFacturaComponent } from './facturas/detalle-factura.component';
import { FacturaComponent } from './facturas/factura.component';
import { KeycloakAngularModule, KeycloakService } from "keycloak-angular";
import { LoginKeycloakService } from "./services/loginKeycloack/login-keycloak.service";
import { initializeKeycloak } from "./appInit/AppInit";
import { LoginGuardGuard } from './guards/login-guard.guard';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { FooterModule } from './footer/footer.module';
import { HeaderModule } from './header/header.module';

registerLocaleData(localeES, 'es');

const routes: Routes = [
  { path: '', component: ClientesComponent, canActivate :[LoginGuardGuard] },
  { path: '**', redirectTo: '' },
  { path: 'directivas', component: DirectivaComponent , canActivate :[LoginGuardGuard]},
  { path: 'clientes', component: ClientesComponent , canActivate :[LoginGuardGuard]},
  { path: 'clientes/page/:page', component: ClientesComponent, canActivate :[LoginGuardGuard] },
  { path: 'clientes/form', component: FormComponent  , canActivate :[LoginGuardGuard]},
  { path: 'clientes/form/:id', component: FormComponent , canActivate :[LoginGuardGuard] },
 
  { path: 'productos', component: ProductosComponent },
  { path: 'productos/page/:page', component: ProductosComponent },
  { path: 'productos/form', component: ProductosFormComponent  },
  { path: 'productos/form/:id', component: ProductosFormComponent  },

/*
  { path: 'usuarios', component: UsuariosComponent },
  { path: 'usuarios/page/:page', component: UsuariosComponent },
  { path: 'usuarios/form', component: UsuariosFormComponent  },
  { path: 'usuarios/form/:id', component: UsuariosFormComponent  },
  { path: 'usuarios/form/roles/agregar/:id', component: FormRolesAgregarComponent  },
*/
  //{ path: 'login', component: LoginComponent },

  { path: 'facturas/:id', component: DetalleFacturaComponent },
  { path: 'facturas/form/:clienteId', component: FacturaComponent  }
];
const AppRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full', },
  { path: '', component: AdminLayoutComponent, children: [{ path: '', loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule' }], canActivate :[LoginGuardGuard] },
  { path: '**', redirectTo: 'home' }];

@NgModule({
  declarations: [

    AppComponent,
    AdminLayoutComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(AppRoutes,
      { useHash: true }),
    BrowserAnimationsModule, MatDatepickerModule, MatMomentDateModule,
    KeycloakAngularModule,
    FooterModule,
    HeaderModule
  ],
    providers: [ClienteService,ProveedorService,
      KeycloakService,
      {
        provide: APP_INITIALIZER,
        useFactory: initializeKeycloak,
        multi: true,
        deps: [KeycloakService]
      },
      LoginKeycloakService
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
