import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { KeycloakService } from 'keycloak-angular';
import { LoginGuardGuard } from '../../guards/login-guard.guard';

@Injectable({
  providedIn: 'root'
})
export class LoginKeycloakService {
  username: string;
  userDataLogin: any = {};

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  urlUserData = environment.baseUrl + 'apibancaria/api/usuario/userdata';

  constructor(private keycloak: KeycloakService,private httpClient: HttpClient,private login: LoginGuardGuard) { }

rol:any;
  logout() {
    this.keycloak.logout();
  }
  getRolUser() {
    let rol_activo;
    this.rol = this.login.getRol();
    this.rol.forEach(elemt => {
      if (elemt === 'realm-user') {
        rol_activo = 'realm-user';
      } else if (elemt == "realm-admin") {
        rol_activo = 'realm-admin';
      }


    })
    return rol_activo;
  }
  redirecToProfile() {
   this.keycloak.getKeycloakInstance().accountManagement();
  }

  getRoles(): string[] {
    console.log(this.keycloak.getUserRoles());
    return this.keycloak.getUserRoles();
  }

  public userData(): any {
   /* const token = this.keycloak.getToken();
    this.userDataLogin = this.httpClient.get(this.urlUserData, { headers: new HttpHeaders({ 'Authorization': `Bearer ${token}` }) })
    */
  
   
    return this.userDataLogin;

  }
}
