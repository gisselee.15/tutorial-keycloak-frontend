import { TestBed } from '@angular/core/testing';

import { LoginKeycloakService } from './login-keycloak.service';

describe('LoginKeycloakService', () => {
  let service: LoginKeycloakService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoginKeycloakService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
