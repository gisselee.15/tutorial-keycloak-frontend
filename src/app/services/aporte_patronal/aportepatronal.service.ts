
/**
 * @author Sergio Amarilla
 * @since 21-03-21
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { KeycloakService } from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class AportepatronalService {

  httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  url_aporte_patronal = environment.baseUrl + 'apibancaria/api/aporte';

  protected token;

  // se inserta las dependenia que se nesecitan dentro del servicios
  constructor(private httpClient: HttpClient, private loginService: KeycloakService) { }



  /**
   * proceso para guardar el pdf y los montos correspondientes a los bancos
   * @param aporte_patronal 
   * @param file 
   * @returns el ok si se guarda el pdf en el servidor
   */
  /*guardarAportePatronal(aporte_patronal, file) {

    const formData = new FormData()
    formData.append('aporte', aporte_patronal)
    formData.append('file', (file))
    const token = this.loginService.getToken();
    return this.httpClient.post(this.url_aporte_patronal, formData, { headers: new HttpHeaders({ 'Authorization': `Bearer ${token}` }) })
  }*/

 /* guardarAportePatronalTxt(aporte_id, file_txt) {
    const formData = new FormData();
    formData.append('file', file_txt);
    const token = this.loginService.getToken();
    return this.httpClient.post(this.url_aporte_patronal + '/' + aporte_id + '/detalle', formData, { headers: new HttpHeaders({ 'Authorization': `Bearer ${token}` }) })
  }*/

  /**
   * listado de aportes
   * @param page 
   * @param size 
   * @param order 
   * @param asc 
   * @param buscar 
   * @returns listado de aportes and search
   */

 /* getDataAportePatronal(page: number, size: number, order: string, asc: boolean, buscar: number) {

    const token = this.loginService.getToken();
    if (buscar == undefined) {
      return this.httpClient.get(this.url_aporte_patronal + `?page=${page}&size=${size}&order=${order}&asc=${asc}`
        , { headers: new HttpHeaders({ 'Authorization': `Bearer ${token}` }) })
    } else {
      return this.httpClient.get(this.url_aporte_patronal + `?size=${size}&order=${order}&asc=${asc}&search=nroOperacion==${buscar}`
        , { headers: new HttpHeaders({ 'Authorization': `Bearer ${token}` }) })
    }
  }*/


}
