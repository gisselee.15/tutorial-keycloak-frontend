import { TestBed } from '@angular/core/testing';

import { AportepatronalService } from './aportepatronal.service';

describe('AportepatronalService', () => {
  let service: AportepatronalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AportepatronalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
