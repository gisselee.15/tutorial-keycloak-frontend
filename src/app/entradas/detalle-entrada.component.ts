import { Component, OnInit } from '@angular/core';
import { EntradaService } from './services/entrada.service';
import { Entrada } from './models/entrada';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalle-entrada',
  templateUrl: './detalle-entrada.component.html'
})
export class DetalleEntradaComponent implements OnInit {

  entrada: Entrada;
  titulo: string = 'Entrada';

  constructor(private entradaService: EntradaService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      this.entradaService.getEntrada(id).subscribe(entrada => this.entrada = entrada);
    });
  }

}
