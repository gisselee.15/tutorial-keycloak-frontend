import { Producto } from './producto';
import { Entrada } from './entrada';

export class ItemEntrada {
  producto: Producto;
  cantidad: number = 1;
  importe: number;

  public calcularImporte(): number {
    this.importe = 0;
    this.importe = this.cantidad * this.producto.precio;//cambiar a costo
  
    return this.importe;  
  }

  
}
