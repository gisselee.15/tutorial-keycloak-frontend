import { ItemEntrada } from './item-entrada';

export class Entrada {
  id: number;
  descripcion: string;
  observacion: string;
  items: Array<ItemEntrada> = [];
  total: number;
  createAt: string;
  usuario: string;
  calcularGranTotal(): number {
    this.total = 0;
    this.items.forEach((item: ItemEntrada) => {
      this.total += item.calcularImporte();
    });
    return this.total;
  }
}
