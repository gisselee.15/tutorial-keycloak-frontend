import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Entrada } from '../models/entrada';
import { Caja } from '../models/caja';

import { Producto } from '../models/producto';
import { map, catchError, tap } from 'rxjs/operators';
import { ItemEntrada } from '../models/item-entrada';

@Injectable({
  providedIn: 'root'
})
export class EntradaService {

  private urlEndPoint: string = 'http://localhost:8080/api/entradas';
  private urlEndPointItem: string = 'http://localhost:8080/api/item-entrada';

  private urlEndPointCaja: string = 'http://localhost:8080/api/caja';
  constructor(private http: HttpClient) { }

  getEntradaPage(page: number): Observable<any> {
    return this.http.get(this.urlEndPoint + '/page/' + page).pipe(
      tap((response: any) => {
        console.log('facturaService: tap 1');
        (response.content as Entrada[]).forEach(entrada => console.log(entrada.descripcion));
      }),
      map((response: any) => {
        (response.content as Entrada[]).map(entrada => {
          entrada.descripcion = entrada.descripcion.toUpperCase();
          return entrada;
        });
        return response;
      }),
      tap(response => {
        console.log('EntradaService: tap 2');
        (response.content as Entrada[]).forEach(entrada => console.log(entrada.descripcion));
      }));
  }
  /*getItemEntradaPage(page: number): Observable<any> {
    return this.http.get(this.urlEndPointItem + '/page/' + page).pipe(
      tap((response: any) => {
        console.log('facturaService: tap 1');
        (response.content as ItemEntrada[]).forEach(itemEntrada => console.log(itemEntrada.facturaId));
      }),
      map((response: any) => {
        (response.content as ItemEntrada[]).map(itemEntrada => {
          itemEntrada.estado =  itemEntrada.estado;
          return itemEntrada;
        });
        return response;
      }),
      tap(response => {
        console.log('EntradaService: tap 2');
        (response.content as ItemEntrada[]).forEach(itemEntrada => console.log(itemEntrada.estado));
      }));
  }*/
  getEntrada(id: number): Observable<Entrada> {
    return this.http.get<Entrada>(`${this.urlEndPoint}/${id}`);
  }

  getCaja(username: string): Observable<any> {
    return this.http.get(this.urlEndPointCaja + '/username/' + username).pipe(
      tap((response: any) => {}),
      map((response: any) => {
        (response as Caja[]).map(caja => {
          return caja;
        });
        return response;
      }),
      tap(response => { }));
  }
  getSumatoriaVentas(username: string, fecha: string): Observable<any> { //PORBAR 
    return this.http.get(this.urlEndPointCaja+ '/ventas/' + username + '/'+fecha).pipe(
      tap((response: any) => {}),
      map((response: any) => {
        
        return response;
      }),
      tap(response => { }));
  }

  /*updateItem(cliente: ItemEntrada): Observable<any> {
    return this.http.put<any>(`${this.urlEndPointItem}/${cliente.id}`, cliente).pipe(
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }));
  }*/
  delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.urlEndPoint}/${id}`);
  }

  filtrarProductos(term: string): Observable<Producto[]> {
    return this.http.get<Producto[]>(`${this.urlEndPoint}/filtrar-productos/${term}`);
  }

  create(entrada: Entrada): Observable<Entrada> {
    return this.http.post<Entrada>(this.urlEndPoint, entrada);
  }

  createCaja(caja: Caja): Observable<Caja> {
    return this.http.post<Caja>(this.urlEndPointCaja, caja);
  }

  updateCaja(cliente: Caja): Observable<any> {
    return this.http.put<any>(`${this.urlEndPointCaja}/${cliente.id}`, cliente).pipe(
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      }));
  }
}
