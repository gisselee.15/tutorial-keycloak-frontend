import { Component, OnInit, Input } from '@angular/core';
import { FacturaService } from '../facturas/services/factura.service';
import { Factura } from './models/factura';
import { ActivatedRoute } from '@angular/router';
import { map, catchError, tap } from 'rxjs/operators';
import { ItemFactura } from './models/items-factura';

import swal from 'sweetalert2';
import { ModalService } from '../clientes/detalle/modal.service';
import { Cliente } from '../clientes/cliente';
import { ClienteService } from '../clientes/cliente.service';

@Component({
  selector: 'app-listado-pedidos-vista',
  templateUrl: './listado-pedidos-vista.component.html'
})

export class ListadoPedidosVistaComponent implements OnInit {

@Input() cliente: Cliente;
facturas: Factura[];
listaFacturas: Factura []= new Array<Factura>(); 
paginador: any;
facturaSeleccionado: Factura;
id: any;

constructor(private clienteService: ClienteService,
  private facturaService: FacturaService,
  private modalService: ModalService,
  private activatedRoute: ActivatedRoute) { }
  ngOnInit() {
  
    //DESCOMENTAR funcion  setInterval PARA CARGAR LISTA ACTUALZIADA 

    this.id = setInterval(() => {
      this.listarFacturas(); 
    }, 5000);
  }
  listarFacturas() {
    this.listaFacturas= new Array<Factura>(); 
    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');

      if (!page) {
        page = 0;
      }

      this.facturaService.getFacturaPage(page)
        .pipe().subscribe(response => {// filtrar los pedidos en preparados y en proceso o listo 
          this.facturas = response.content as Factura[];
          this.paginador = response;

          if(this.facturas.length>0){
            for (let factura of this.facturas){
               
              var entre=(factura.items).findIndex(i => i.estado === "entregado");
              var proc=(factura.items).findIndex(i => i.estado === "en proceso");
              var lis=(factura.items).findIndex(i => i.estado === "listo");

              if(entre==-1){//si no se encuentra entregado
                if(proc==-1){// si no hay ninguno en listo en proceso
                  
                  if(lis==-1){// si no hay ninguno en listo
                    factura.estado="pendiente";
                  }else{
                    factura.estado="listo";
                  }
                }else{// si alguno es en proceso sin importar el resto
                  factura.estado="en proceso";
                }
              }else{
                factura.estado="entregado";
              }
             /* if(factura.estado=="en proceso" || factura.estado=="listo"  ){
                this.listaFacturas.push(factura);
              }*/
            }  
          }
        });
    });
  }
  cerrarModal() {
    this.modalService.cerrarModal();
  }

  /*delete(factura: Factura): void {
    swal({
      title: 'Está seguro?',
      text: `¿Seguro que desea anular la factura ${factura.descripcion}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, anular!',
      cancelButtonText: 'No, cancelar!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.facturaService.delete(factura.id).subscribe(
          () => {
            this.cliente.facturas = this.cliente.facturas.filter(f => f !== factura)
            swal(
              'Factura Eliminada!',
              `Factura ${factura.descripcion} eliminada con éxito.`,
              'success'
            )
          }
        )

      }
    });
  }*/

/*export class ListadoPedidosVistaComponent implements OnInit {
  errores: string[];
  factura: Factura;
  titulo: string = 'Factura';
  itemFactura: ItemFactura[];
  paginador: any;
  constructor(private facturaService: FacturaService,
    private activatedRoute: ActivatedRoute,
    private modalService: ModalService) { }
id: any;
  ngOnInit() {
    

    this.listar();
//DESCOMENTAR funcion  setInterval PARA CARGAR LISTA ACTUALZIADA 

    this.id = setInterval(() => {
      this.listar(); 
    }, 1000);
  }
listar(){

  this.activatedRoute.paramMap.subscribe(params => {
    let page: number = +params.get('page');

    if (!page) {
      page = 0;
    }

    this.facturaService.getItemFacturaPage(page)
      .pipe(
        tap(response => {
          console.log('FacturaComponent: tap 3');
          (response.content as ItemFactura[]).forEach(itemFactura => console.log(itemFactura.importe));
        })
      ).subscribe(response => {
        this.itemFactura = response.content as ItemFactura[];
        this.paginador = response;
      });
  });


}
  entregar(itemFactura: ItemFactura): void {
    swal({
      title: 'Está seguro?',
      text: `¿Desea entregar el pedido ${itemFactura.id} del cliente ${itemFactura.nombre}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si!',
      cancelButtonText: 'No!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
          console.log(itemFactura);
          //this.cliente.facturas = null;
          itemFactura.estado='entregado';
          this.facturaService.updateItem(itemFactura)
            .subscribe(
              json => {
                //this.router.navigate(['/clientes']);
                swal('Item Actualizado', `${json.mensaje}: ${json.id}-${json.nombre}`, 'success');
                this.ngOnInit();
              },
              err => {
                this.errores = err.error.errors as string[];
                console.error('Código del error desde el backend: ' + err.status);
                console.error(err.error.errors);
              }
            )
      
      }
    });
  }
  abrirModal() {
    this.modalService.abrirModal();
  }
*/

}
