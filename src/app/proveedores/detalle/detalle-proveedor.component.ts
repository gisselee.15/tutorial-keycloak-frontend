import { Component, OnInit, Input } from '@angular/core';
import { Proveedor } from '../proveedor';
import { ProveedorService } from '../proveedor.service';
import { ModalProveedorService } from './modal-proveedor.service';

import swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';

import { CompraService } from '../../compras/services/compra.service';
import { Compra } from '../../compras/models/compra';

@Component({
  selector: 'detalle-proveedor',
  templateUrl: './detalle-proveedor.component.html',
  styleUrls: ['./detalle-proveedor.component.css']
})
export class DetalleProveedorComponent implements OnInit {

  @Input() proveedor: Proveedor;

  titulo: string = "Detalle del Ticket";
  private fotoSeleccionada: File;
  progreso: number = 0;

  constructor(private proveedorService: ProveedorService,
    private compraService: CompraService,
    private modalService: ModalProveedorService) { }

  ngOnInit() { }

  seleccionarFoto(event) {
    /*this.fotoSeleccionada = event.target.files[0];
    this.progreso = 0;
    console.log(this.fotoSeleccionada);
    if (this.fotoSeleccionada.type.indexOf('image') < 0) {
      swal('Error seleccionar imagen: ', 'El archivo debe ser del tipo imagen', 'error');
      this.fotoSeleccionada = null;
    }*/
  }

  subirFoto() {

    /*if (!this.fotoSeleccionada) {
      swal('Error Upload: ', 'Debe seleccionar una foto', 'error');
    } else {
      this.proveedorService.subirFoto(this.fotoSeleccionada, this.proveedor.id)
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progreso = Math.round((event.loaded / event.total) * 100);
          } else if (event.type === HttpEventType.Response) {
            let response: any = event.body;
            this.proveedor = response.proveedor as Proveedor;

            this.modalService.notificarUpload.emit(this.proveedor);
            swal('La foto se ha subido completamente!', response.mensaje, 'success');
          }
        });
    }*/
  }

  cerrarModal() {
    this.modalService.cerrarModal();
    this.fotoSeleccionada = null;
    this.progreso = 0;
  }

  delete(Compra: Compra): void {
    swal({
      title: 'Está seguro?',
      text: `¿Seguro que desea eliminar la Compra ${Compra.descripcion}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.compraService.delete(Compra.id).subscribe(
          () => {
            this.proveedor.compras = this.proveedor.compras.filter(f => f !== Compra)
            swal(
              'Compra Eliminada!',
              `Compra ${Compra.descripcion} eliminada con éxito.`,
              'success'
            )
          }
        )

      }
    });
  }

}
