import { Component, OnInit } from '@angular/core';
import { Proveedor } from './proveedor';
import { Region } from './region';
import { ProveedorService } from './proveedor.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form-proveedores',
  templateUrl: './form-proveedores.component.html'
})
export class FormProveedoresComponent implements OnInit {

  private proveedor: Proveedor = new Proveedor();
  regiones: Region[];
  titulo: string = "Crear Proveedor";

  errores: string[];

  constructor(private proveedorService: ProveedorService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let id = +params.get('id');
      if (id) {
        this.proveedorService.getProveedor(id).subscribe((proveedor) => this.proveedor = proveedor);
      }
    });

    this.proveedorService.getRegiones().subscribe(regiones => this.regiones = regiones);
  }

  create(): void {
    console.log(this.proveedor);
    this.proveedorService.create(this.proveedor)
      .subscribe(
        proveedor => {
          this.router.navigate(['/proveedores']);
          swal('Nuevo proveedor', `El proveedor  ha sido creado con éxito`, 'success');
        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
          swal('Nuevo proveedor', ` ${err.error.mensaje} , ${err.error.error} `, 'error');

        }
      );
  }

  update(): void {
    console.log(this.proveedor);
    this.proveedor.compras = null;
    this.proveedorService.update(this.proveedor)
      .subscribe(
        json => {
          this.router.navigate(['/proveedores']);
          swal('Proveedor Actualizado', `El proveedor  ha sido actualizado con éxito`, 'success');
        },
        err => {
          this.errores = err.error.errors as string[];
          console.error('Código del error desde el backend: ' + err.status);
          console.error(err.error.errors);
        }
      )
  }

  compararRegion(o1: Region, o2: Region): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }

    return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.id === o2.id;
  }

}
