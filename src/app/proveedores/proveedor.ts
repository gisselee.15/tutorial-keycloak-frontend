import { Region } from './region';
import { Compra } from '../compras/models/compra';

export class Proveedor {
  id: number;
  nombre: string;
  encargado: string;
  createAt: string;
  email: string;
  foto: string;
  region: Region;
  cedula: string;
  telefono: number;
  direccion: string;
  compras: Array<Compra> = [];
}
