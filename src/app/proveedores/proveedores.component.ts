import { Component, OnInit } from '@angular/core';
import { Proveedor } from './proveedor';
import { ProveedorService } from './proveedor.service';
import { ModalProveedorService } from './detalle/modal-proveedor.service';
import swal from 'sweetalert2';
import { tap } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html'
})
export class ProveedoresComponent implements OnInit {

  proveedors: Proveedor[];
  paginador: any;
  proveedorSeleccionado: Proveedor;

  constructor(private proveedorService: ProveedorService,
    private modalService: ModalProveedorService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');

      if (!page) {
        page = 0;
      }

      this.proveedorService.getProveedors(page)
        .pipe(
          tap(response => {
            console.log('ProveedorsComponent: tap 3');
            (response.content as Proveedor[]).forEach(proveedor => console.log(proveedor.nombre));
          })
        ).subscribe(response => {
          this.proveedors = response.content as Proveedor[];
          this.paginador = response;
        });
    });

    this.modalService.notificarUpload.subscribe(proveedor => {
      this.proveedors = this.proveedors.map(proveedorOriginal => {
        if (proveedor.id == proveedorOriginal.id) {
          proveedorOriginal.foto = proveedor.foto;
        }
        return proveedorOriginal;
      })
    })
  }

  delete(proveedor: Proveedor): void {
    swal({
      title: 'Está seguro?',
      text: `¿Seguro que desea eliminar al proveedor ${proveedor.nombre} ${proveedor.encargado}?`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'No, cancelar!',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
      reverseButtons: true
    }).then((result) => {
      if (result.value) {

        this.proveedorService.delete(proveedor.id).subscribe(
          () => {
            this.proveedors = this.proveedors.filter(cli => cli !== proveedor)
            swal(
              'Proveedor Eliminado!',
              `Proveedor ${proveedor.nombre} eliminado con éxito.`,
              'success'
            )
          }
        )

      }
    });
  }

  abrirModal(proveedor: Proveedor) {
    this.proveedorSeleccionado = proveedor;
    this.modalService.abrirModal();
  }

}
